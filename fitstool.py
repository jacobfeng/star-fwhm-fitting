from pylab import *
from glob import *
import pyfits
import os
import sys
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import figure, show
import numpy
from scipy.optimize import curve_fit
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText

def gauss1(x, *p):
    A, mu, sigma, offset = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))+offset
        
class ZoomPan:
    def __init__(self, data):
        self.press = None
        self.cur_xlim = None
        self.cur_ylim = None
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.xpress = None
        self.ypress = None
        self.data = data
        self.ymax, self.xmax = data.shape
    
    def zoom_factory(self, ax, ax_x, ax_y, base_scale = 2.):
        def zoom(event):
            cur_xlim = ax.get_xlim()
            cur_ylim = ax.get_ylim()

            xdata = event.xdata # get event x location
            ydata = event.ydata # get event y location

            if event.button == 'up':
                # deal with zoom in
                scale_factor = 1 / base_scale
            elif event.button == 'down':
                # deal with zoom out
                scale_factor = base_scale
            else:
                # deal with something that should never happen
                scale_factor = 1
                print event.button

            new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
            new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

            relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
            rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

            xl = floor(xdata - new_width * (1-relx))
            if xl < 0:
                xl = 0
            xh = ceil(xdata + new_width * (relx))
            if xh > self.xmax:
                xh = self.xmax
            xm = round((xl+xh)/2);
            yl = floor(ydata - new_height * (1-rely))
            if yl < 0:
                yl = 0
            yh = ceil(ydata + new_height * (rely))
            if yh > self.ymax:
                yh = self.ymax
            ym = round((yl+yh)/2);
            
            xvalue = self.data[ym,xl:xh];
            yvalue = self.data[yl:yh,xm];
                       
            xopt = None
            yopt = None
            
            if 2*median(xvalue) < max(xvalue):
                func, p0 = gauss1, [1., xm, 10, min(xvalue)]
                try:
                    xopt, var_matrix = curve_fit(func, arange(xl,xh), xvalue, p0=p0)
                    if var_matrix[0][0]==inf:
                        xopt = None
                except:
                    pass
                    
            if 2*median(yvalue) < max(yvalue):
                func, p0 = gauss1, [1., ym, 10, min(yvalue)]
                try:
                    yopt, var_matrix = curve_fit(func, arange(yl,yh), yvalue, p0=p0)
                    if var_matrix[0][0]==inf:
                        yopt = None
                except:
                    pass
             
            ax_y.cla();
            ax_x.cla();
            ax.cla();
            ax.imshow(self.data)
            
            ax_y.plot(yvalue,arange(yl,yh),'x')
            ax_y.set_ylim(self.cur_ylim)
            ax_x.plot(arange(xl,xh),xvalue,'x')
            ax_x.set_xlim(self.cur_xlim)    
            
            if xopt is not None:
                newx = linspace(xl, xh, 100);
                ax_x.plot(newx, gauss1(newx, *xopt),'r');
                at = AnchoredText("coefficient: %.0f,%.0f,%.0f,%.0f;\nFWHM: %.1f"%(xopt[0],xopt[1],abs(xopt[2]),xopt[3],abs(xopt[2])*2.35482),
                  prop=dict(size=8), frameon=True,
                  loc=2,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax_x.add_artist(at)
            if yopt is not None:
                newy = linspace(yl, yh, 100);
                ax_y.plot(gauss1(newy, *yopt), newy,'r');
                at = AnchoredText("coefficient: %.0f,%.0f,%.0f,%.0f;\nFWHM: %.1f"%(yopt[0],yopt[1],abs(yopt[2]),yopt[3],abs(yopt[2])*2.35482),
                  prop=dict(size=8), frameon=True,
                  loc=1,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax_y.add_artist(at)
            
                            
            if (xopt is not None) and (yopt is not None):
                background_value = min([xopt[3],yopt[3]])
                data_patch = self.data[yl:yh,xl:xh]
                row, col = data_patch.shape
                flux = sum(data_patch) - row*col*background_value 
                at = AnchoredText("flux: %.0f"%(flux),
                  prop=dict(size=8), frameon=True,
                  loc=3,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax.add_artist(at)

            ax.set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
            ax_x.set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
            ax.set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
            ax_y.set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
            
            ax.axvline(xm, color='r');
            ax.axhline(ym, color='r');

            ax_y.grid(color='red', which='both');
            ax_x.grid(color='red', which='both');
            ax.grid(color='red',which='both');
            ax.figure.canvas.draw()
            
        fig = ax.get_figure() # get the figure of interest
        fig.canvas.mpl_connect('scroll_event', zoom)
        
        return zoom

    def pan_factory(self, ax, ax_x, ax_y):
        def onPress(event):
            if event.inaxes != ax: return
            self.cur_xlim = ax.get_xlim()
            self.cur_ylim = ax.get_ylim()
            self.press = self.x0, self.y0, event.xdata, event.ydata
            self.x0, self.y0, self.xpress, self.ypress = self.press

        def onRelease(event):
            self.press = None
            ax.grid(color='red',which='both');
            ax.figure.canvas.draw()

        def onMotion(event):
            if event.inaxes != ax: return
            if self.press is None: return
            dx = event.xdata - self.xpress
            dy = event.ydata - self.ypress
            self.cur_xlim -= dx
            self.cur_ylim -= dy
            
            xl = floor(self.cur_xlim[0])
            if xl < 0:
                xl = 0
            xh = ceil(self.cur_xlim[1])
            if xh > self.xmax:
                xh = self.xmax
            xm = round((xl+xh)/2);
            yl = floor(self.cur_ylim[0])
            if yl < 0:
                yl = 0
            yh = ceil(self.cur_ylim[1])
            if yh > self.ymax:
                yh = self.ymax
            ym = round((yl+yh)/2);
            
            xvalue = self.data[ym,xl:xh];
            yvalue = self.data[yl:yh,xm];
            
            ax_y.cla()
            ax_x.cla()
            ax.cla();
            ax.imshow(self.data)
            
            ax_y.plot(yvalue,arange(yl,yh),'x')
            ax_y.grid(color='red', which='both')
            
            ax_x.plot(arange(xl,xh),xvalue,'x')
            ax_x.grid(color='red', which='both')
            
            xopt = None
            yopt = None
            
            if 2*median(xvalue) < max(xvalue):
                func, p0 = gauss1, [1., xm, 10, min(xvalue)]
                try:
                    xopt, var_matrix = curve_fit(func, arange(xl,xh), xvalue, p0=p0)
                    if var_matrix[0][0]==inf:
                        xopt = None
                except:
                    pass
            if 2*median(yvalue) < max(yvalue):
                func, p0 = gauss1, [1., ym, 10, min(yvalue)]
                try:
                    yopt, var_matrix = curve_fit(func, arange(yl,yh), yvalue, p0=p0)
                    if var_matrix[0][0]==inf:
                        yopt = None
                except:
                    pass
                                   
            if xopt is not None:
                newx = linspace(xl, xh, 100);
                ax_x.plot(newx, gauss1(newx, *xopt),'r');
                at = AnchoredText("coefficient: %.0f,%.0f,%.0f,%.0f;\nFWHM: %.1f"%(xopt[0],xopt[1],abs(xopt[2]),xopt[3],abs(xopt[2])*2.35482),
                  prop=dict(size=8), frameon=True,
                  loc=2,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax_x.add_artist(at)
            if yopt is not None:
                newy = linspace(yl, yh, 100);
                ax_y.plot(gauss1(newy, *yopt), newy,'r');
                at = AnchoredText("coefficient: %.0f,%.0f,%.0f,%.0f;\nFWHM: %.1f"%(yopt[0],yopt[1],abs(yopt[2]),yopt[3],abs(yopt[2])*2.35482),
                  prop=dict(size=8), frameon=True,
                  loc=1,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax_y.add_artist(at)
                
            if (xopt is not None) and (yopt is not None):
                background_value = min([xopt[3],yopt[3]])
                data_patch = self.data[yl:yh,xl:xh]
                row, col = data_patch.shape
                flux = sum(data_patch) - row*col*background_value 
                at = AnchoredText("flux: %.0f"%(flux),
                  prop=dict(size=8), frameon=True,
                  loc=3,
                  )
                at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
                ax.add_artist(at)                
                
            ax.set_xlim(self.cur_xlim)
            ax.set_ylim(self.cur_ylim)
            
            ax_x.set_xlim(self.cur_xlim)
            ax_y.set_ylim(self.cur_ylim)
            
            ax.grid(color='red',which='both');
            
            ax.axvline(xm, color='r');
            ax.axhline(ym, color='r');
            
            ax.figure.canvas.draw()

        fig = ax.get_figure() # get the figure of interest

        # attach the call back
        fig.canvas.mpl_connect('button_press_event',onPress)
        fig.canvas.mpl_connect('button_release_event',onRelease)
        fig.canvas.mpl_connect('motion_notify_event',onMotion)
        
        #return the function
        return onMotion


if __name__ == "__main__":
    if (len(sys.argv)==2 and sys.argv[1]=='-h') or len(sys.argv)==1:
        print "photometry.py <data fits file> [main back fits file]"
        sys.exit(1)

    # read in file data
    filename = sys.argv[1]
    hdulist = pyfits.open(filename)
    data = hdulist[0].data
    hdulist.close()
    
    if len(sys.argv)==3:
        backmain_filename = sys.argv[2]
        hdulist = pyfits.open(backmain_filename)
        backmain_data = hdulist[0].data
        hdulist.close()
        try:
            data = data - backmain_data
        except:
            sys.exit(1)

    #data = reshape(arange(0,512*512), (512, 512))
    nullfmt = NullFormatter()         # no labels

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left+width+0.005

    rect_image = [left, bottom, width, height]
    rect_x = [left, bottom_h, width, 0.2]
    rect_y = [left_h, bottom, 0.2, height]
    
    # start with a rectangular Figure
    plt.figure(1, figsize=(9,9))

    ax_image = plt.axes(rect_image)
    ax_x = plt.axes(rect_x)
    ax_y = plt.axes(rect_y)
    
    # no labels
    ax_x.xaxis.set_major_formatter(nullfmt)
    ax_y.yaxis.set_major_formatter(nullfmt)

    ax_image.imshow(data, extent = [0,512, 0,512])
    ax_image.set_xlim([0,512])
    ax_image.set_ylim([0,512])
    ax_image.grid(color='red',which='both');

    ax_y.plot(flipud(data[:,256]),arange(0,512))
    ax_y.set_ylim([0,512])
    ax_y.grid(color='red',which='both');

    ax_x.plot(data[256,:])
    ax_x.set_xlim([0,512])
    ax_x.grid(color='red',which='both');
    
    scale = 1.1
    zp = ZoomPan(flipud(data))
    figZoom = zp.zoom_factory(ax_image, ax_x, ax_y, base_scale = scale)
    figPan = zp.pan_factory(ax_image, ax_x, ax_y)

    show()
